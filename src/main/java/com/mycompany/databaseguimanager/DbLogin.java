package com.mycompany.databaseguimanager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

/**
 * The `DbLogin` class represents a user interface for logging into a database.
 * It allows the user to enter data such as username, password, and database
 * name, and then attempt a login.
 *
 * @author Karol Madejski
 * @version 1.0
 */
public class DbLogin extends JFrame {

    private JTextField usernameField;
    private JPasswordField passwordField;
    private JTextField databaseNameField;
    private JButton loginButton;

    static String databaseName;
    static String username;
    static String password;

    /**
     * The `DbLogin` class represents a user interface for logging into a
     * database. It allows the user to enter data such as username, password,
     * and database name, and then attempt a login.
     */
    public DbLogin() {
        setTitle("Database Login");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new GridLayout(5, 3));
        initializeComponents();
        setupLoginButton();

        pack();
        setLocationRelativeTo(null); // Center the frame on the screen
        setVisible(true);
    }

    /**
     * Initializes the user interface components, including text fields where
     * user can fill out database name, username, password fields and the login
     * button.
     */
    private void initializeComponents() {
        JLabel databaseNameLabel = new JLabel("Database:");
        databaseNameField = new JTextField();
        JLabel usernameLabel = new JLabel("Username:");
        usernameField = new JTextField();
        JLabel passwordLabel = new JLabel("Password:");
        passwordField = new JPasswordField();

        loginButton = new JButton("Login");

        add(databaseNameLabel);
        add(databaseNameField);
        add(usernameLabel);
        add(usernameField);
        add(passwordLabel);
        add(passwordField);
        add(loginButton);
    }

    /**
     * Sets up the login button to handle login attempts. After the button is
     * clicked method retrieve login details and proceed the panel class.
     */
    private void setupLoginButton() {
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                retrieveLoginDetails();
                hideLoginWindow();
                openPanel();
            }
        });
    }

    /**
     * Retrieves the login details (database name, username and password)
     * entered by the user from the input fields.
     */
    private void retrieveLoginDetails() {
        databaseName = databaseNameField.getText();
        username = usernameField.getText();
        password = new String(passwordField.getPassword());

        //Debugging
        System.out.println("Database: " + databaseName);
        System.out.println("Username: " + username);
        System.out.println("Password: " + password);
    }

    /**
     * Hides the login window.
     */
    private void hideLoginWindow() {
        setVisible(false); // Hide the login window
        dispose();
    }

    /**
     * Opens the Panel class
     */
    private void openPanel() {
        SwingUtilities.invokeLater(Panel::new);
    }

    /**
     * Retrieves a database connection using the provided login details
     * (database name, username, and password).
     *
     * @return A connection to the database.
     * @throws SQLException if there is an issue establishing the database
     * connection.
     */
    public static Connection getConnection() throws SQLException {
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + databaseName, username, password);
        return connection;
    }

    /**
     * Gets the currently set database name.
     *
     * @return The database name.
     */
    public String getDatabaseName() {
        System.out.println("getDatabaseName: " + databaseName);
        return databaseName;
    }

    /**
     * Gets the currently set username.
     *
     * @return The username.
     */
    public String getUsername() {
        System.out.println("getUsername: " + username);
        return username;
    }

    /**
     * Gets the currently set password.
     *
     * @return The password.
     */
    public String getPassword() {
        System.out.println("getPassword: " + password);
        return password;
    }
}
