package com.mycompany.databaseguimanager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The `Panel` class represents the main user menu interface for the Database GUI
 * Manager. It allows the user to interact with the database, including
 * selecting tables and running custom queries.
 */
public class Panel extends JFrame {

    private DbLogin dbLogin;
    private Connection connection;

    private JTextField tableNameField;
    private JComboBox<String> tableComboBox;
    private JButton loginButton;
    private JTextField customQueryField;
    private JButton queryConfirmButton;
    private JButton reportsButton;

    static String tableName;
    static String customQuery;

    /**
     * Constructs the Database GUI Manager panel.
     */
    public Panel() {
        setTitle("Panel Database Manager");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new GridLayout(12, 3));

        // Initialize user interface components
        JLabel tableNameLabel = new JLabel("Table:");
        tableNameField = new JTextField();
        tableComboBox = new JComboBox<>();
        loginButton = new JButton("Login");
        JLabel queryNameLabel = new JLabel("Custom Query:");
        customQueryField = new JTextField();
        queryConfirmButton = new JButton("Zatwierdź");
        reportsButton = new JButton("Reports");

        initializeTableComboBox();

        // Add components to the layout
        add(tableNameLabel);
        add(tableNameField);
        add(tableComboBox);
        add(new JLabel());
        add(loginButton);
        add(new JLabel());
        add(queryNameLabel);
        add(customQueryField);
        add(new JLabel());
        add(queryConfirmButton);
        add(new JLabel());
        add(reportsButton);

        dbLogin = new DbLogin();

        // Register action listeners
        tableComboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                updateTableName(null, null);
            }
        });

        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateTableName(null, null);
                hideLoginWindow();
                openDatabaseGuiManager();
            }
        });

        queryConfirmButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                customQuery = customQueryField.getText();
                hideLoginWindow();
                openDatabaseGuiManager();
            }
        });

        reportsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                setVisible(false); // Hide the login window
                hideLoginWindow();
                openReports();
            }
        });

        pack();
        setLocationRelativeTo(null); // Center the frame on the screen
        setVisible(true);
    }
    /**
     * Initializes the table selection combo box by populating it with table names retrieved from the database.
     */
    private void initializeTableComboBox() {
        List<String> tables = getTablesFromDatabase();
        for (String table : tables) {
            tableComboBox.addItem(table);
        }
    }
    /**
     * Retrieves a list of table names from the database.
     *
     * @return A list of table names.
     */
    private List<String> getTablesFromDatabase() {
        List<String> tables = new ArrayList<>();

        try {
            connection = dbLogin.getConnection();
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet resultSet = metaData.getTables(null, null, null, new String[]{"TABLE"});

            while (resultSet.next()) {
                String tableName = resultSet.getString("TABLE_NAME");
                tables.add(tableName);
            }

            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new Error("Problem at Panel/getTablesFromDatabase");
        }
        return tables;
    }
    /**
     * Updates the selected table name based on user input.
     *
     * @param textField The text field value.
     * @param comboBox  The combo box value.
     */
    private void updateTableName(String textField, String comboBox) {
        String databaseName = dbLogin.getDatabaseName();
        textField = tableNameField.getText();
        comboBox = (String) tableComboBox.getSelectedItem();
        if (!"".equals(textField) || !textField.isEmpty() || !textField.isBlank()) {
            tableName = textField;
            System.out.println("Selected Table by Field: " + tableName + databaseName);
        } else if (comboBox != null) {
            tableName = comboBox;
            
            System.out.println("Selected Table by CB: " + tableName + databaseName);
        } else {
            tableName = (String) tableComboBox.getItemAt(0);
            System.out.println("Selected Table by Default: " + tableName + databaseName);
        }
    }
    /**
     * Hides the login window.
     */
    private void hideLoginWindow() {
        setVisible(false); // Hide the login window
        dispose(); // Dispose the login window

    }
    /**
     * Opens the Database GUI Manager class.
     */
    private void openDatabaseGuiManager() {
        SwingUtilities.invokeLater(DatabaseGuiManager::new);
    }
    /**
     * Opens the report class - unused for now.
     */
    private void openReports() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                Reports reports = new Reports();
                reports.generateReports(connection);
            }
        });
    }

    /**
     * Gets the currently selected table name.
     *
     * @return The selected table name.
     */
    public String getTableName() {
        System.out.println("gettable: " + tableName);
        return tableName;
    }

    /**
     * Gets the custom query entered by the user.
     *
     * @return The custom query.
     */
    public String getCustomQuery() {
        System.out.println("get custom query: " + customQuery);

        return customQuery;

    }
}
