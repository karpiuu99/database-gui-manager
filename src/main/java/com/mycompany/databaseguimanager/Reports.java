package com.mycompany.databaseguimanager;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Reports extends Application {

    private TextArea textArea;
    private Connection connection;
    private ScheduledExecutorService executorService;

    public Reports() {
    }

    public void generateReports(Connection connection) {
        this.connection = connection;
        launch();
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Real-Time Database Reports");
        System.out.println("start działa ");

        textArea = new TextArea();
        textArea.setEditable(false);

        StackPane root = new StackPane();
        root.getChildren().add(textArea);

        primaryStage.setScene(new Scene(root, 400, 300));
        primaryStage.show();

        System.out.println("Executor 1.");
        executorService = Executors.newSingleThreadScheduledExecutor();
        System.out.println("Executor 2.");
        executorService.scheduleAtFixedRate(this::checkForChanges, 0, 1, TimeUnit.SECONDS);
    }

    private void checkForChanges() {
        System.out.println("checkforchanges");
        // Zapytanie SQL do monitorowania zmian w tabeli 'item_changes'
        String query = "SELECT itemId, newObjName, oldObjName FROM item_changes";

        try {
            System.out.println("Try 0");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            System.out.println("Try");

            while (resultSet.next()) {

                int itemId = resultSet.getInt("itemId");
                String newObjName = resultSet.getString("newObjName");
                String oldObjName = resultSet.getString("oldObjName");
                System.out.println("While result next działa: " + itemId + newObjName + oldObjName);

                Platform.runLater(() -> {
                    System.out.println("RunLater działa: " + itemId + newObjName + oldObjName);
                    textArea.appendText(oldObjName + " is just trading " + itemId + "with " + newObjName);
                });
            }

            statement.close();
        } catch (SQLException e) {
            System.out.println("catch");

            e.printStackTrace();
        }
    }

    @Override
    public void stop() {
        // Zakończenie nasłuchiwania i zamknięcie połączenia z bazą danych
        if (executorService != null) {
            executorService.shutdown();
        }

        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
