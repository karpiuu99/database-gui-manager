package com.mycompany.databaseguimanager;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.sql.*;

/**
 * The `DatabaseGuiManager` class represents the main graphical user interface
 * for managing a database. It allows users to interact with the database, view
 * and modify table data. This class extends the `JFrame` class and provides
 * functionalities for displaying table data and executing queries.
 */
public class DatabaseGuiManager extends JFrame {

    private Connection connection;
    private JTable table;
    private DbLogin dbLogin;
    private Panel panel;
    private String tableName;
    private String databaseName;
    private String user;
    private String password;
    private String customQuery;

    /**
     * Constructs the `DatabaseGuiManager` class. Initializes
     * the GUI, establishes a database connection, fetches and displays
     * table data.
     */
    public DatabaseGuiManager() {
        dbLogin = new DbLogin();
        panel = new Panel();
        tableName = panel.getTableName();
        databaseName = dbLogin.getDatabaseName();
        customQuery = panel.getCustomQuery();
        System.out.println("custom po wywołaniu getCustomQuery " + customQuery);

        initGUI();
        initializeConnection();
        fetchAndCreateTableData();
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        //closeConnection(connection);
    }

    /**
     * Initializes GUI settings.
     */
    private void initGUI() {
        setTitle("Database GUI MANAGER");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
    }

    /**
     * Initializes the database connection using the credentials provided by
     * `DbLogin`.
     */
    private void initializeConnection() {
        try {
            connection = dbLogin.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Fetches and creates table data to be displayed in the interface.
     * Depending on whether a custom query is provided, it either retrieves data
     * from a specific table or executes the custom query.
     */
    private void fetchAndCreateTableData() {
        try {
            if (customQuery == null || customQuery.isEmpty()) {
                Object[][] data = fetchData(connection);
                String[] columnNames = getColumnNames(connection);
                createTable(data, columnNames);
            } else {
                executeCustomQuery(connection, customQuery);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Closes the database connection if it is not null.
     *
     * @param connection The database connection to be closed.
     */
    private void closeConnection(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Retrieves data from the database based on the provided connection and
     * custom query (if specified).
     *
     * @param connection The database connection to be used for fetching data.
     * @return A two-dimensional array of Objects containing the retrieved data.
     * @throws SQLException If a database error occurs during data retrieval.
     */
    private Object[][] fetchData(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet;

        if (customQuery == null || customQuery.isEmpty()) {
            System.out.println("custom query null or empty" + customQuery);
            resultSet = statement.executeQuery("SELECT * FROM " + tableName);
        } else {
            System.out.println("custom query cośma " + customQuery);
            resultSet = statement.executeQuery(customQuery);
        }

        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();
        Object[][] data = new Object[10000][columnCount];

        int row = 0;
        while (resultSet.next()) {
            for (int col = 1; col <= columnCount; col++) {
                data[row][col - 1] = resultSet.getObject(col);
            }
            row++;
        }
        resultSet.close();
        statement.close();
        return data;
    }

    /**
     * Retrieves column names from the database for the specified table.
     *
     * @param connection The database connection to be used for fetching column
     * names.
     * @return An array of String containing the column names.
     * @throws SQLException If a database error occurs during column name
     * retrieval.
     */
    private String[] getColumnNames(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT * FROM " + tableName);

//        if (customQuery == null || customQuery.isEmpty()) {
//        } else {
        //   resultSet = statement.executeQuery(customQuery);   
        //}
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();

        String[] columnNames = new String[columnCount];
        for (int i = 1; i <= columnCount; i++) {
            columnNames[i - 1] = metaData.getColumnName(i);
        }
        resultSet.close();
        statement.close();
        return columnNames;
    }

    /**
     * The `createTable` method generates a table using the retrieved data and
     * column names. It allows cell editing and handles changes made to the
     * table data.
     *
     * @param data A two-dimensional array of Objects representing the table
     * data.
     * @param columnNames An array of String containing the column names.
     */
    private void createTable(Object[][] data, String[] columnNames) {
        DefaultTableModel model = new DefaultTableModel(data, columnNames) {
            public boolean isCellEditable(int row, int column) {
                return true;
            }
        };

        table = new JTable(model);
        table.getModel().addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                if (e.getType() == TableModelEvent.UPDATE) {
                    int row = e.getFirstRow();
                    int column = e.getColumn();
                    Object newValue = table.getValueAt(row, column);
                    try {
                        updateData(connection, row, column, newValue);
                        //connection.close();
                    } catch (SQLException ex) {
                        System.out.println("Problem in tableChanged");
                        ex.printStackTrace();

                    }
                }
            }
        });

        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane, BorderLayout.CENTER);
    }

    private void updateData(Connection connection, int row, int column, Object newValue) throws SQLException {
        String columnName = table.getColumnName(column);
        Object primaryKeyValue = table.getValueAt(row, 0); // Example: first column is the primary key
        String primaryColumnName = table.getColumnName(0);

        String updateQuery = "UPDATE " + tableName + " SET " + columnName + " = ? WHERE " + primaryColumnName + " = ?";

        try (PreparedStatement statement = connection.prepareStatement(updateQuery)) {
            statement.setObject(1, newValue);
            statement.setObject(2, primaryKeyValue);
            statement.executeUpdate();
        } catch (Exception ex) {
            System.out.println("Problem in updateData");
            ex.printStackTrace();
        }
    }

    /**
     * Executes a custom SQL query provided by the user. The query results are
     * displayed in the interface if it returns a result set, or a message
     * dialog shows the number of affected rows for other types of queries.
     *
     * @param connection The database connection to execute the query on.
     * @param customQuery The custom SQL query to be executed.
     */
    private void executeCustomQuery(Connection connection, String customQuery) {
        try {
            Statement statement = connection.createStatement();
            boolean hasResultSet = statement.execute(customQuery);

            if (hasResultSet) {
                ResultSet resultSet = statement.getResultSet();
                ResultSetMetaData metaData = resultSet.getMetaData();
                int columnCount = metaData.getColumnCount();

                String[] columnNames = new String[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    columnNames[i - 1] = metaData.getColumnName(i);
                }

                Object[][] data = new Object[10000][columnCount];
                int row = 0;
                while (resultSet.next()) {
                    for (int col = 1; col <= columnCount; col++) {
                        data[row][col - 1] = resultSet.getObject(col);
                    }
                    row++;
                }

                resultSet.close();
                statement.close();

                createTable(data, columnNames);
            } else {
                int updatedRowCount = statement.getUpdateCount();
                JOptionPane.showMessageDialog(null, "Query executed succesfully");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error executing query: " + e.getMessage());
        }
    }

}
